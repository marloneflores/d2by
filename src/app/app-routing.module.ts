import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { MyInventoryComponent } from './my-account/my-inventory/my-inventory.component';
import { MyMessageComponent } from './my-account/my-message/my-message.component';
import { MyPlayComponent } from './my-account/my-play/my-play.component';
import { MyReflinkComponent } from './my-account/my-reflink/my-reflink.component';
import { MySteamUrlComponent } from './my-account/my-steam-url/my-steam-url.component';

import { D2byInventoryComponent } from './my-account/my-inventory/d2by-inventory/d2by-inventory.component';
import { GiftHistoryComponent } from './my-account/my-inventory/gift-history/gift-history.component';
import { InventoryHistoryComponent } from './my-account/my-inventory/inventory-history/inventory-history.component';
import { ConvertCoinsComponent } from './my-account/my-inventory/convert-coins/convert-coins.component';
import { SteamInventoryComponent } from './my-account/my-inventory/steam-inventory/steam-inventory.component';
import { MyGameComponent } from './my-account/my-play/my-game/my-game.component';
import { MyCaseComponent } from './my-account/my-play/my-case/my-case.component';
import { CashBackComponent } from './my-account/my-play/cash-back/cash-back.component';
import { RegisterPageComponent } from './auth/register-page/register-page.component';
import { EventsNewsComponent } from './events/events-news/events-news.component';
import { EventsBlogsComponent } from './events/events-blogs/events-blogs.component';
import { EventsComponent } from './events/events.component';
import { MatchesComponent } from './matches/matches.component';
import { D2byStoreComponent } from './my-account/d2by-store/d2by-store.component';
import { TopWinComponent } from './matches/top-win/top-win.component';
import { TopEarnComponent } from './matches/top-earn/top-earn.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { AuthGuard } from './auth/auth.guard';

const appRoutes: Routes = [
    {path: '', redirectTo : 'homepage', pathMatch: 'full'},
    { path:'homepage', component: HomepageComponent},
    { path: 'my-account', component: MyAccountComponent,  children : [
        {path : 'my-inventory', component: MyInventoryComponent, canActivate:[AuthGuard], children : [
            {path : 'steam-inventory', component : SteamInventoryComponent},
            {path : 'd2by-inventory', component : D2byInventoryComponent},
            {path : 'gift-history', component : GiftHistoryComponent},
            {path : 'inventory-history', component : InventoryHistoryComponent},
            {path : 'convert-coins', component : ConvertCoinsComponent},
        ]},
        { path : 'store', component : D2byStoreComponent },
        {path : 'my-message', component: MyMessageComponent},
        {path : 'my-play', component: MyPlayComponent ,canActivate:[AuthGuard], children: [
            {path: 'my-game', component : MyGameComponent},
            {path: 'my-case', component : MyCaseComponent},
            {path: 'cash-back', component : CashBackComponent}
        ]},
        {path : 'my-reflink', component: MyReflinkComponent},
        {path : 'my-steam-url', component: MySteamUrlComponent},
    ]},
    { path : 'events', component : EventsComponent, children: [
        { path: 'news', component: EventsNewsComponent},
        { path: 'blogs', component: EventsBlogsComponent}
    ]},
    { path : 'matches', component : MatchesComponent , children: [
        { path: 'win', component: TopWinComponent},
        { path: 'earn', component : TopEarnComponent}
    ]},
    { path: 'login', component: LoginPageComponent},
    { path: 'register', component: RegisterPageComponent},
    {path: '**', redirectTo : 'homepage', pathMatch: 'full'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports : [RouterModule]
})

export class AppRoutingModule{

}