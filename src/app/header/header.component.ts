import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy{
  toggleEvent : boolean = true;
  private _opened: boolean = false;
  private userSub : Subscription;
  isLoggedin= false;
  toggleD2BY = true;
  private _toggleOpened(): void {
    this._opened = !this._opened;
  }

  constructor(private authService : AuthService, private router: Router) { }

  ngOnInit() {

    this.userSub = this.authService.user.subscribe(user => {
      this.isLoggedin = !!user;

      console.log(!!user);
    });
    this.authService.autoLogin();

  }
  
  onToggleEvents() {
    this.toggleEvent =  !this.toggleEvent;
    
  }
  onToggleD2BY() {
    this.toggleD2BY = !this.toggleD2BY
  }



  onLogout(){
    this.authService.logout();
  }

  ngOnDestroy(){
    this.userSub.unsubscribe();
  }
 
}
