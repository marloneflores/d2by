import { Component, OnInit } from '@angular/core';
import { BlogService } from '../events-blogs/events-blogs.service';
import { Blogs } from '../events-blogs/events-blogs.model';

@Component({
  selector: 'app-events-news',
  templateUrl: './events-news.component.html',
  styleUrls: ['./events-news.component.css']
})
export class EventsNewsComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }

}
