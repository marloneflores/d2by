import { Injectable } from '@angular/core';
import { Blogs } from './events-blogs.model';

@Injectable({providedIn:"root"})
export class BlogService {
    constructor() {}

    blogs : Blogs[] = 
    [new Blogs (
        'https://dota2bestyolo.com/upload/blog/news_1571993128047.jpg',
        'Halloween ">[Giveaway] Spooky Halloween',
        'https://dota2bestyolo.com/upload/avatars/file_1571905652375.png',
        'The Daredevil',
        '2019-10-25 16:44',
        'View: 16249'
    ),
    new Blogs (
        'https://dota2bestyolo.com/upload/blog/z1569830495961_692129027b5dd0d892ff0d891240ccd1_1570622973607.jpg',
        'Dota2BestYolo\s Website Update: News',
        'https://dota2bestyolo.com/upload/avatars/file_1541152619581.png',
        'The Kingpin',
        '2019-10-09 20:09',
        'View: 2836'
    ),
    new Blogs (
        'https://dota2bestyolo.com/upload/blog/bl_1570531154080.jpg',
        'Dota2BestYolo Update Changelogs 2019',
        'https://dota2bestyolo.com/upload/avatars/file_1541152619581.png',
        'The Kingpin',
        '2019-10-08 18:39',
        'View: 1161'
    ),
    new Blogs (
        'https://dota2bestyolo.com/upload/blog/Untitled-2_1568022562571.jpg',
        'The returning of Dota 2 Case!',
        'https://dota2bestyolo.com/upload/avatars/file_1541152619581.png',
        'The Kingpin',
        '2019-09-09 17:49',
        'View: 2090'
    ),
    new Blogs (
        'https://dota2bestyolo.com/upload/blog/blog_1566374236348.jpg',
        '[Giveaway] Trove Carafe 2019',
        'https://dota2bestyolo.com/upload/avatars/file_1541152619581.png',
        'The Kingpin',
        '2019-08-21 15:57',
        'View: 5648'
    ),
    new Blogs (
        'https://dota2bestyolo.com/upload/blog/blog_1566295327772.jpg',
        'Loyal\'s Tribution - Honey Heist Baby Roshan Giveaway!',
        'https://dota2bestyolo.com/upload/avatars/file_1541152619581.png',
        'The Kingpin',
        '2019-08-20 18:02',
        'View: 2614'
    )
]

getBlogs(){
    return this.blogs;
}
    

}