export class Blogs {
    constructor(
        public image : string, 
        public title: string, 
        public authorImg: string, 
        public author: string,
        public date: string, 
        public view:string)
        {

        }
}