import { Component, OnInit } from '@angular/core';
import { Blogs } from './events-blogs.model';
import { BlogService } from './events-blogs.service';

@Component({
  selector: 'app-events-blogs',
  templateUrl: './events-blogs.component.html',
  styleUrls: ['./events-blogs.component.css']
})
export class EventsBlogsComponent implements OnInit {
  blogs : Blogs[];
  constructor(private blogService: BlogService) { }

  ngOnInit() {
    this.blogs = this.blogService.getBlogs()
  }

}
