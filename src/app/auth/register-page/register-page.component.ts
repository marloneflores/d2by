import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from './must-match.validator';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  error : string;
  isLoading = false;
  registerForm : FormGroup;
  submitted : boolean = false;
  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group ({
        email : new FormControl (null, [Validators.required, Validators.email]),
        nickname : new FormControl (null, [Validators.required]),
        password : new FormControl (null, [Validators.required, Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,30})')]),
        confirmPassword : new FormControl (null, [Validators.required]),
        acceptTerms : new FormControl (false, [Validators.requiredTrue])
    },{ validator: MustMatch('password', 'confirmPassword')});
  }
  get f() { return this.registerForm.controls; }

  onSubmit(){
    this.submitted= true;
    this.isLoading = true;

    
    console.log(this.registerForm.controls.email.errors);
    const email = this.registerForm.controls.email.value;
    const password = this.registerForm.controls.password.value;

    this.authService.register(email, password).subscribe(resData => {
      console.log(resData)
      this.router.navigate(['/login'])
      this.isLoading = false;
    }, errorMessage =>{
      this.error = errorMessage;
      this.isLoading = false;
      console.log(errorMessage);
    });

  }
 
}
