import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  loginForm : FormGroup;
  isLoading= false;
  submitted = false;
  error: string;
  constructor(private formBuilder : FormBuilder, private authService: AuthService, private router : Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
          email : new FormControl (null, [Validators.email, Validators.required]),
         password : new FormControl (null, [Validators.required, Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,30})')])
    })
  }
  get f(){
    return this.loginForm.controls;
  }
  onSubmit(){
    this.submitted = true;
    this.isLoading = true;
    const email = this.loginForm.controls.email.value;
    const password = this.loginForm.controls.password.value;

    this.authService.login(email, password).subscribe(resData => {
        this.isLoading = false;
        this.router.navigate(['homepage']);
        console.log(resData);
      },errorMessage => {
        this.error = errorMessage;
        console.log(this.error);
        this.isLoading = false;
      }
    )
  }

}
