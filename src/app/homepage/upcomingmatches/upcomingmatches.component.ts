import { Component, OnInit } from '@angular/core';
import { Matches } from './matches.model';
import { UpcomingMatchesService } from './upcomingmatches.service';

@Component({
  selector: 'app-upcomingmatches',
  templateUrl: './upcomingmatches.component.html',
  styleUrls: ['./upcomingmatches.component.css']
})
export class UpcomingmatchesComponent implements OnInit {
  filteredGame : string;

  matches :Matches[] ;

  
  constructor(private umService : UpcomingMatchesService) { }

  ngOnInit() {
    this.filteredGame = 'all';
    this.matches = this.umService.getMatches();
  }

  filterAll(){
    this.filteredGame = 'all';
  }
  filterDota(){
    this.filteredGame = 'dota';
  }

  filterCSGO(){
    this.filteredGame = 'csgo';
  }
  filterLOL(){
    this.filteredGame = 'lol';
  }

  filterFootball(){
    this.filteredGame = 'football';
  }

  filterBasketball(){
    this.filteredGame = 'basketball'
  }

}

