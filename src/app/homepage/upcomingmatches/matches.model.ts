export class Matches{
    constructor(
        public game: string,
        public logo : string,
        public tournament:string , 
        public time:string, 
        public team1: string, 
        public team1odds: string, 
        public bo:string,
        public team2: string, 
        public team2odds: string, 
        public tLogo: string
        )
        {}
}