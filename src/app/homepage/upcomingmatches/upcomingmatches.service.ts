import { Injectable } from '@angular/core';
import { Matches } from './matches.model';

@Injectable({providedIn:"root"})
export class UpcomingMatchesService {
    matches : Matches[] =  [
        new Matches(
          'csgo',
          'https://dota2bestyolo.com/images/icon/icon-csgo-match.png',
         'StarSeries I-League Season 8',
         '1h from now',
         'https://dota2bestyolo.com/upload/team/vitality_1539705151649_1559279284642.jpg',
         '1 : 0.21',
         'BO1',
         'https://dota2bestyolo.com/upload/team/Renegades_1491812785233.jpg',
         '1 : 4.28',
         'https://dota2bestyolo.com/upload/tournament/800px_starseries_i_league_csgo_1569551859819.jpg'
         ),
          new Matches(
            'lol',
              'https://dota2bestyolo.com/images/icon/icon-lol-match.png',
             '2019 World Championship',
             '14h from now',
             'https://dota2bestyolo.com/upload/team/600px_funplus_phoenixlogo_square_1569203291799.jpg',
             '1 : 1.03',
             'VS',
             'https://dota2bestyolo.com/upload/team/g2_1494851983075_1559284627906.jpg',
             '1 : 0.87',
             'https://dota2bestyolo.com/upload/tournament/worlds_qh0utymo9muwdv2fnyg8_1200x630_1570006785944.jpg'
           ),
           new Matches(
            'dota',
            'https://dota2bestyolo.com/images/icon/icon-dota2-match.png',
           'ESL ONE Hamburg 2019',
           '7h from now',
           'https://dota2bestyolo.com/upload/team/Alliance_1491985084324_1520579944007.jpg',
           '1 : 0.36',
           'BO2',
           'https://dota2bestyolo.com/upload/team/600px_virtus_1552383962864_1559277468802.jpg',
           '1 : 2.48',
           'https://dota2bestyolo.com/upload/tournament/600px_esl_one_hamburg_2019_1569052294418.jpg'
           ) ,
           
           new Matches(
             'football',
             'https://dota2bestyolo.com/images/icon/icon-sport-match.png',
            'UEFA Europa League 2019/2020',
            '8h from now',
            'https://dota2bestyolo.com/upload/team/FC_Porto_1506417879604.jpg',
            '1 : 0.84',
            'VS',
            'https://dota2bestyolo.com/upload/team/rangers_international_fc_plc_1565269732817.jpg',
            '1 : 1.07',
            'https://dota2bestyolo.com/upload/tournament/uel_large_850x525_1565269416742.jpg'
            ), 
            new Matches(
              'dota',
             'https://dota2bestyolo.com/images/icon/icon-dota2-match.png',
             'ESL ONE Hamburg 2019',
             '2h from now',
             'https://dota2bestyolo.com/upload/team/tnc_predator_logo_201907_1564379520934.jpg',
             '1 : 0.31',
             'BO2',
             'https://dota2bestyolo.com/upload/team/Wind And Rain_1527534386831.jpg',
             '1 : 2.79',
             'https://dota2bestyolo.com/upload/tournament/600px_esl_one_hamburg_2019_1569052294418.jpg'
             ),
            new Matches(
              'basketball',
              'https://dota2bestyolo.com/images/icon/icon-basketball-match.png',
             'NBA - REGULAR SEASON',
             '14h from now',
             'https://dota2bestyolo.com/upload/team/Detroit Pistons_1509754888474.jpg',
             '1 : 0.86',
             'BO5',
             'https://dota2bestyolo.com/upload/team/atl_1509798798384.jpg',
             '1 : 1.04',
             'https://dota2bestyolo.com/upload/tournament/nba_1571751551330.jpg'
             )
           
        ]
    constructor(){}

        getMatches(){
            return this.matches;
        }
}