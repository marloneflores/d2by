import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  myInterval = 1500;
  activeSlideIndex = 0;
 
  slides = [
    {image: 'https://dota2bestyolo.com/upload/slider/z1569830491716_dc07f0bfe66f73373d6db1749051cf48_1570623388061.jpg'},
    {image: 'https://dota2bestyolo.com/upload/slider/z1568419181606_0302f598733e8cfe7f7817690e6bc47b_1570531236500.jpg'},
    {image: 'https://dota2bestyolo.com/upload/slider/banner-web-895x450_1570524869893.jpg'},
    {image: 'https://dota2bestyolo.com/upload/slider/banner%20(1)_1571027840024.jpg'},
    
    
  ];
  constructor() { }

  ngOnInit() {
  }

}
