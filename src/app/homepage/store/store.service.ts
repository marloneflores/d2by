import { Injectable } from "@angular/core";
import { StoreItem } from './store-item.model';

@Injectable({providedIn:'root'})
export class StoreService {
    storeItem : StoreItem[] = [
        new StoreItem(
            'https://dota2bestyolo.com/game_dota2/pauldrons_of_eternal_harvest.png',
            'Mythical',
            'Pauldrons of Eternal Harvest',
            '182'
        ),
        new StoreItem(
            'https://dota2bestyolo.com/game_dota2/kantusa_the_script_sword.png',
            'Mythical',
            'Genuine Kantusa the Script Sword',
            '164'
        ),
        new StoreItem(
            'https://dota2bestyolo.com/game_dota2/cult_of_the_demon_trickster.png',
            'Immortal',
            'Cult of the Demon Trickster',
            '108'
        ),
        new StoreItem(
            'https://dota2bestyolo.com/game_dota2/armor_of_the_demon_trickster.png',
            'Immortal',
            'Armor of the Demon Trickster',
            '78'
        ),
        new StoreItem(
            'https://dota2bestyolo.com/game_dota2/vigil_triumph.png',
            'Immortal',
            'Vigil Triumph',
            '76'
        )
    ];
      
    getStoreItem(){
        return this.storeItem;
    }
}