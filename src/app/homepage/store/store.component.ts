import { Component, OnInit } from '@angular/core';
import { StoreItem } from './store-item.model';
import { StoreService } from './store.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  items : StoreItem[];
  constructor(private storeService : StoreService) { }

  ngOnInit() {
    this.items = this.storeService.getStoreItem();
  }

}
