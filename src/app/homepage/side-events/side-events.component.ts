import { Component, OnInit } from '@angular/core';
import { Events } from './events.model';

@Component({
  selector: 'app-side-events',
  templateUrl: './side-events.component.html',
  styleUrls: ['./side-events.component.css']
})
export class SideEventsComponent implements OnInit {
  events : Events[] = [
    new Events(
      '[Giveaway] Spooky Halloween ',
      'https://dota2bestyolo.com/upload/blog/z1591788056767_6fbfb69f8355a50d54c1a20c7b5ca5b5_1571993084451.jpg',
      "Halloween is near Dota2BestYolo is haunted. Deep inside, there're presents for the Wanderers who knock on the door at midnight. Promo codes for those who seek, but only 50 Wanderers can claim each night. Claimed ones must not stay for more, or else The Kingpin &amp; The Daredevil also opened 2 feasts Roll Giveaway, tribute the Spirits and to celebrate this year's event. To hide from the Wicked Devil, they decided to create a passcode and restrictions. Make sure to light a bonfire and stay the night at Dota2BestYolo. It'd be a scary lonely night - said the Wanderers",
    ),
    new Events(
      'DIRETIDE DOTA 2 - THE HALLOWEEN EVENT WE HAVE ALL BEEN WAITING FOR',
      'https://dota2bestyolo.com/images/events/home-bg/1.jpg',
      "Diretide Dota 2 is an annual tournament event that takes place in the online multiplayer strategy game DotA 2. The event was held in celebration of Halloween holiday and was organized by Valve, the developer behind the game and was described as a new “annual” event for players to enjoy new maps and game modes"),
      new Events(
        'Dota2BestYolo\'s Website Update: News',
        'https://dota2bestyolo.com/upload/blog/z1560057964011_d1c034f3fd0483f9bc74b8f28fa9d568_1570622973611.jpg',
        'Dear valued customers. As the recent update is settled, we\'ve tracked down numerous bugs as the user\'s deposits were doubled/tripled. Users who exploited the error had their Golden Coins corrected today. Those who abused repeatedly trying to take advantage of the bug is banned'),
        new Events(
          'Dota2BestYolo Update  Changelogs 2019 ',
          'https://dota2bestyolo.com/upload/blog/z1557350763763_2cd36960f8d7f2fad685f79799775f31_1570531154087.jpg',
          "#Update #Changelogs  Dota 2 Best Yolo  1. Golden Coins: Before the update, the conversion ratio when you deposit items is $1 = 1000 coins. Now after update, the conversion ratio will be $1 = 1 coins. All current coins will be scaled accordingly, there is no change of real value "),
          
  ]



  constructor() { }

  ngOnInit() {
  }

}
