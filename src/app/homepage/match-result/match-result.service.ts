import { Injectable } from '@angular/core';
import { MatchResult } from './match-result.model';

@Injectable({providedIn:"root"})
export class MatchResultService {
    matches : MatchResult[] =  [
      
        new MatchResult(
        'csgo',
        'https://dota2bestyolo.com/images/icon/icon-csgo-match.png',
        'ESL Pro League Season 10',
        'https://dota2bestyolo.com/upload/team/552px_lucid_dream_2019logo_1557829095598.jpg',
        '2 : 1',
        'https://dota2bestyolo.com/upload/team/7966_1573603388067.jpg',
        '2019-11-15 21:15'
        ),
        new MatchResult(
        'csgo',
        'https://dota2bestyolo.com/images/icon/icon-csgo-match.png',
        'WePlay! Forge of Masters Season 2',
        'https://dota2bestyolo.com/upload/team/3cdc05c0199726df89e8b7fa0504e552_1550010652537.jpg',
        '2 : 0',
        'https://dota2bestyolo.com/upload/team/600px_teamgamerlegion_1554017998960.jpg',
        '2019-11-15 21:07'
        ),
        new MatchResult(
        'basketball',
        'https://dota2bestyolo.com/images/icon/icon-basketball-match.png',
        'NBA - REGULAR SEASON',
        'https://dota2bestyolo.com/upload/team/Denver Nuggets_1509754939838.jpg',
        '101 : 93',
        'https://dota2bestyolo.com/upload/team/Brooklyn Nets_1509983830559.jpg',
        '2019-11-15 14:09'
        ),
        new MatchResult(
        'basketball',
        'https://dota2bestyolo.com/images/icon/icon-basketball-match.png',
        'NBA - REGULAR SEASON',
        'https://dota2bestyolo.com/upload/team/suns-logo_on-blk_720x497-min_1509799041253.jpg',
        '128 : 112',
        'https://dota2bestyolo.com/upload/team/atl_1509798798384.jpg',
        '2019-11-15 12:40'
        ), 
        new MatchResult(
            'football',
            'https://dota2bestyolo.com/images/icon/icon-sport-match.png',
            'EURO - Qualification',
            'https://dota2bestyolo.com/upload/team/France_1497011405451.jpg',
            '2 : 1',
            'https://dota2bestyolo.com/upload/team/1714_1553250345966.jpg',
            '2019-11-15 05:59'
            ),
        new MatchResult(
            'dota',
            'https://dota2bestyolo.com/images/icon/icon-dota2-match.png',
            'JBO Asian Masters League S2',
            'https://dota2bestyolo.com/upload/team/600px_revive_logo_1572389631395.jpg',
            '2 : 0',
            'https://dota2bestyolo.com/upload/team/Keen Gaming_1513379077227.jpg',
            '2019-11-14 17:03'
            ),
        new MatchResult(
        'lol',
        'https://dota2bestyolo.com/images/icon/icon-lol-match.png',
        '2019 World Championship',
        'https://dota2bestyolo.com/upload/team/600px_funplus_phoenixlogo_square_1569203291799.jpg',
        '3 : 0',
        'https://dota2bestyolo.com/upload/team/g2_1494851983075_1559284627906.jpg',
        '2019-11-10 23:32'
        )
        ];
    constructor(){}

        getMatches(){
            return this.matches;
        }
}