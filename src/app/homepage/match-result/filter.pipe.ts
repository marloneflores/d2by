import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filteredGame: string): any {
    if(value.length === 0 || filteredGame === 'all'){
      return value;
    }
    const gameArray = [];
    
    for(const item of value){
      if(item.game === filteredGame){
        gameArray.push(item);
      }
      
    }
    return gameArray;
  }

}
