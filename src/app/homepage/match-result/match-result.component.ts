import { Component, OnInit } from '@angular/core';
import { MatchResult } from './match-result.model';
import { MatchResultService } from './match-result.service';

@Component({
  selector: 'app-match-result',
  templateUrl: './match-result.component.html',
  styleUrls: ['./match-result.component.css']
})
export class MatchResultComponent {
  matches : MatchResult[];
  filteredGame : string;
  constructor(public matchResultService: MatchResultService) { }

  ngOnInit() {
    this.matches = this.matchResultService.getMatches();
    this.filteredGame = 'all';
  }



  filterAll(){
    this.filteredGame = 'all';
  }
  filterDota(){
    this.filteredGame = 'dota';
  }

  filterCSGO(){
    this.filteredGame = 'csgo';
  }
  filterLOL(){
    this.filteredGame = 'lol';
  }

  filterFootball(){
    this.filteredGame = 'football';
  }

  filterBasketball(){
    this.filteredGame = 'basketball'
  }

}
