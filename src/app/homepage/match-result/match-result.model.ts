export class MatchResult {
    constructor(
        public game: string,
        public logo : string,
        public tournament:string ,
        public team1: string, 
        public score:string,
        public team2: string,
        public date: string
        )
        {}
}