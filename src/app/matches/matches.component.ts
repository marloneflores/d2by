import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {
  isLoggedin: boolean;

  constructor(private authService : AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.isLoggedin = !!user;
      console.log(!!user);
      
    })
  }

}
