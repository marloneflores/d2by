import { Component, OnInit } from '@angular/core';
import { Earner } from './earner.model';
import { TopEarnService } from './top-earn.service';

@Component({
  selector: 'app-top-earn',
  templateUrl: './top-earn.component.html',
  styleUrls: ['./top-earn.component.css']
})
export class TopEarnComponent implements OnInit {
  earners : Earner[];
  constructor(private topEarnService: TopEarnService) { }

  ngOnInit() {
    this.earners = this.topEarnService.getEarner();
  }

}
