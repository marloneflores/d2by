import { Injectable } from '@angular/core';
import { Earner } from './earner.model';

@Injectable({providedIn:"root"})
export class TopEarnService {
    earners : Earner[] = [
        new Earner (
            '1',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/f2/f2649c31c3b472e39a7d5d46656b50ed8adaeae9_medium.jpg',
            'WIGGLE DAT',
            'Lv 2081',
            '106,189.131'
        ),        new Earner (
            '2',
            'https://i.imgur.com/7o0dp3B.jpg',
            './.',
            'Lv 4637',
            '85,989.757'
        ),        new Earner (
            '3',
            'https://i.imgur.com/7o0dp3B.jpg',
            'ZeaL',
            'Lv 6249',
            '75,861.697'
        ),        new Earner (
            '4',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/05/05aa5599819bdd6e587896988f3ff4e74e150183_medium.jpg',
            'PANTERA STORE',
            'Lv 2539',
            '67,825.241'
        ),
        new Earner (
            '5',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/d9/d9fb4673362b9b63991c6d878fa14f3762c2f275_medium.jpg',
            ' CheNz ',
            'Lv 1090',
            '49,532.991'
        ),
        new Earner (
            '6',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/23/23b0e4f8060524e682de00e81b22e61d1c4dc79e_medium.jpg',
            '[A]requiem4god',
            'Lv 10519',
            '46,790.841'
        ),
        new Earner (
            '7',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/62/629907b58159331620935d3c02a93a43ce1c423c_medium.jpg',
            'Azzam Store',
            'Lv 1391',
            '41,131.8374'
        ),
        new Earner (
            '8',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/9f/9fb7b50d71ff9eaf137d1c697ccf33a3000564be_medium.jpg',
            '지창',
            'Lv 971',
            '37,600.067'
        ),
       
        new Earner (
            '9',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/96/966f59ba8d8dcfff916e56353b3a15b1e3b42068_medium.jpg',
            'flyingCOW -',
            'Lv 648',
            '37,026.66'
        ),        new Earner (
            '10',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/63/63d69e551123be956daea4ef81eed54485659a70_medium.jpg',
            '...',
            'Lv 2599',
            '36,504.033'
        )
    ];

    getEarner(){
        return this.earners;
    }
}