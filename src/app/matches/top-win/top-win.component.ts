import { Component, OnInit } from '@angular/core';
import { Winner } from './winner.model';
import { TopWinService } from './top-win.service';

@Component({
  selector: 'app-top-win',
  templateUrl: './top-win.component.html',
  styleUrls: ['./top-win.component.css']
})
export class TopWinComponent implements OnInit {
  winners : Winner[];

  constructor(private topWinService: TopWinService) { }

  ngOnInit() {
    this.winners = this.topWinService.getWinner();
  }

}
