import { Injectable } from '@angular/core';
import { Winner } from './winner.model';

@Injectable({providedIn:"root"})
export class TopWinService {
    winner : Winner[] = [
        new Winner (
            '1',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_medium.jpg',
            'Female Monster',
            'Lv 419',
            '206.94'
        ),        new Winner (
            '2',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/05/05ea97122ffea287825c1ff18f90103989f6af41_medium.jpg',
            '†BlooD†',
            'Lv 1576',
            '136.249'
        ),        new Winner (
            '3',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/53/53dd6a0bc14f6218bd37f3f30a6d1b00a0e9ab52_medium.jpg',
            'MaMaMay',
            'Lv 8',
            '110.439'
        ),        new Winner (
            '4',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/e6/e662da43e412456f646edc73a44fbfaedbe90803_medium.jpg',
            'FuziWuzi',
            'Lv 256',
            '86.51'
        ),
        new Winner (
            '5',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/31/3147096f96e88ed1b3db9db72df384bd23e4fe02_medium.jpg',
            'Yoshi',
            'Lv 38',
            '79.616'
        ),
        new Winner (
            '6',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/e0/e0a5a3a7a443d070ae328a25524402acac63d429_medium.jpg',
            '[✖]',
            'Lv 685',
            '75.99'
        ),
        new Winner (
            '7',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/96/966f59ba8d8dcfff916e56353b3a15b1e3b42068_medium.jpg',
            'flyingCOW -',
            'Lv 648',
            '73.549'
        ),
        new Winner (
            '8',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/09/09943ff1901eb3a38c5349594ece9a8875a4276f_medium.jpg',
            '´Dendi',
            'Lv 337',
            '55.997'
        ),
        new Winner (
            '9',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/a5/a57c192ce8b69e809379d9bbf1976dcde5bcd057_medium.jpg',
            'dh ❤ cs.money',
            'Lv 89',
            '48.303'
        ),        new Winner (
            '10',
            'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/54/541bb9ec7c4157ffcddfcacbe633aa950e43dbc0_medium.jpg',
            'AnneAnnePro0',
            'Lv 2',
            '46.639'
        )
    ];

    getWinner(){
        return this.winner;
    }
}