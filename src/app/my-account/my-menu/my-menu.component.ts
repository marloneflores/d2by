import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-my-menu',
  templateUrl: './my-menu.component.html',
  styleUrls: ['./my-menu.component.css']
})
export class MyMenuComponent implements OnInit{
  isLoggedin = false;
  constructor(private authService : AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.isLoggedin = !!user;
      console.log(!!user);
      
    })
  }


}