import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
  isLoggedin : boolean = false;

  constructor(private authService : AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.isLoggedin = !!user;
      console.log(!!user);
    });
  }

}
