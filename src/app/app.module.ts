import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselComponent } from './homepage/carousel/carousel.component';
import { UpcomingmatchesComponent } from './homepage/upcomingmatches/upcomingmatches.component';
import { StoreComponent } from './homepage/store/store.component';
import { SideEventsComponent } from './homepage/side-events/side-events.component';
import { MatchResultComponent } from './homepage/match-result/match-result.component';
import { HeaderComponent } from './header/header.component';
import { HomepageComponent } from './homepage/homepage.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarModule } from 'ng-sidebar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FilterPipe } from './homepage/upcomingmatches/filter.pipe';
import { AppRoutingModule } from './app-routing.module';
import { MyAccountComponent } from './my-account/my-account.component';
import { MyInventoryComponent } from './my-account/my-inventory/my-inventory.component';
import { MyPlayComponent } from './my-account/my-play/my-play.component';
import { MyReflinkComponent } from './my-account/my-reflink/my-reflink.component';
import { MySteamUrlComponent } from './my-account/my-steam-url/my-steam-url.component';
import { MyMessageComponent } from './my-account/my-message/my-message.component';

import { InventoryHistoryComponent } from './my-account/my-inventory/inventory-history/inventory-history.component';
import { GiftHistoryComponent } from './my-account/my-inventory/gift-history/gift-history.component';
import { ConvertCoinsComponent } from './my-account/my-inventory/convert-coins/convert-coins.component';
import { D2byInventoryComponent } from './my-account/my-inventory/d2by-inventory/d2by-inventory.component';
import { SteamInventoryComponent } from './my-account/my-inventory/steam-inventory/steam-inventory.component';
import { MyGameComponent } from './my-account/my-play/my-game/my-game.component';
import { MyCaseComponent } from './my-account/my-play/my-case/my-case.component';
import { CashBackComponent } from './my-account/my-play/cash-back/cash-back.component';
import { RegisterPageComponent } from './auth/register-page/register-page.component';
import { EventsNewsComponent } from './events/events-news/events-news.component';
import { EventsBlogsComponent } from './events/events-blogs/events-blogs.component';
import { EventsComponent } from './events/events.component';
import { MatchesComponent } from './matches/matches.component';
import { D2byStoreComponent } from './my-account/d2by-store/d2by-store.component';
import { TopWinComponent } from './matches/top-win/top-win.component';
import { TopEarnComponent } from './matches/top-earn/top-earn.component';
import { MyMenuComponent } from './my-account/my-menu/my-menu.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CarouselComponent,
    UpcomingmatchesComponent,
    StoreComponent,
    SideEventsComponent,
    MatchResultComponent,
    HomepageComponent,
    FooterComponent,
    FilterPipe,
    LoginPageComponent,
    MyAccountComponent,
    MyInventoryComponent,
    MyPlayComponent,
    MyReflinkComponent,
    MySteamUrlComponent,
    MyMessageComponent,
    SteamInventoryComponent,
    D2byInventoryComponent,
    InventoryHistoryComponent,
    GiftHistoryComponent,
    ConvertCoinsComponent,
    MyGameComponent,
    MyCaseComponent,
    CashBackComponent,
    RegisterPageComponent,
    EventsNewsComponent,
    EventsBlogsComponent,
    EventsComponent,
    MatchesComponent,
    D2byStoreComponent,
    TopWinComponent,
    TopEarnComponent,
    MyMenuComponent,
    LoadingSpinnerComponent


  ],
  imports: [
    BrowserModule,
    SidebarModule.forRoot(),
    CollapseModule.forRoot(),
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    MatNativeDateModule,
    CarouselModule.forRoot(),
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
